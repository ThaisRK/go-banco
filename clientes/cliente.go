package clientes

// Visibilidade: letra maiúscula = public
type Titular struct {
	Pessoa
	Profissao   string
	TipoDeConta string
}

type Dependente struct {
	Pessoa
	Permissoes  []string
	TipoDeConta string
}

type Pessoa struct {
	Nome, Cpf string
}

func (p *Pessoa) VerificaPessoa(cpfInput string) string {

	var msg string

	if p.Cpf == cpfInput {
		msg = ">> Bem-vindo a sua conta <<"
	} else {
		msg = "xx Conta não encontrada xx"
	}

	return msg
}
