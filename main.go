package main

import (
	"fmt"
	"os"

	"gobanco/clientes"
	"gobanco/menu"
)

func PagarBoleto(conta verificarConta, valorDoBoleto float64) {
	conta.Sacar(valorDoBoleto)
}

type verificarConta interface {
	Sacar(valor float64) string
}
type autenticador interface {
	VerificaPessoa(cpf string) string
}

func Logar(cli autenticador, cpfInput string) string {
	msg := cli.VerificaPessoa(cpfInput)
	return msg

}

// Uma interface é a definição de um conjunto de métodos comuns a um ou mais tipos(structs).
// É o que permite a criação de tipos polimórficos em Go.

// Java possui um conceito muito parecido, também chamado de interface.
// A grande diferença é que, em Go, um tipo implementa uma interface implicitamente.
func main() {

	titularInfo := clientes.Pessoa{
		Nome: "Thais",
		Cpf:  "111.111.111-11",
	}

	for {
		menu.ShowMenuAcessar()
		cmd := menu.ReadCmd()

		switch cmd {
		case 1:
			fmt.Println("Insira o CPF:")
			cpfInput := menu.ReadCmdString()

			fmt.Println(Logar(&titularInfo, cpfInput))
		case 0:
			fmt.Println("Saindo...")
			os.Exit(0)
		default:
			fmt.Println("Não conheço esse comando...")
			os.Exit(-1)
		}

	}

}
