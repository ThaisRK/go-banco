package menu

import (
	"fmt"
)

func ShowMenuAcessar() {
	fmt.Println("1 - Acessar Conta")
	fmt.Println("0 - Sair")
}

func ShowMenuOpcoesConta() {
	fmt.Println("1 - Sacar")
	fmt.Println("2 - Depositar")
	fmt.Println("3 - Transfeir")
	fmt.Println("4 - Pagar Boleto")
	fmt.Println("0 - Sair")
}

func ReadCmd() int {
	var cmdInput int
	fmt.Scan(&cmdInput)
	fmt.Println("Comando escolhido: ", cmdInput)
	return cmdInput
}
func ReadCmdString() string {
	var cmdInput string
	fmt.Scan(&cmdInput)
	fmt.Println("O CPF inserido foi:", cmdInput)
	return cmdInput
}
